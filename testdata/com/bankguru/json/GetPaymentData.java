package com.bankguru.json;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GetPaymentData {
	public static GetPaymentData get(String filename) throws JsonParseException, JsonMappingException, IOException  {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(new File(filename), GetPaymentData.class);
	}

	@JsonProperty ("accountType")
	String accountType;
	
	@JsonProperty ("initialDeposit")
	String initialDeposit;
	
	@JsonProperty ("amountDeoposit")
	String amountDeoposit;
	
	@JsonProperty ("amountWithdrawal")
	String amountWithdrawal;	

	@JsonProperty ("amountTransfer")
	String amountTransfer;
	
	@JsonProperty ("accountPayee")
	String accountPayee;	
	
	
	public String getAccountType() {
		return accountType;
	}
	
	public String getInitialDeposit() {
		return initialDeposit;
	}
	
	public String getAmountDeoposit() {
		return amountDeoposit;
	}
	
	public String getAmountWithdrawal() {
		return amountWithdrawal;
	}
	
	public String getAmountTransfer() {
		return amountTransfer;
	}
	
	public String getAccountPayee() {
		return accountPayee;
	}
	
	
}
