package com.bankguru.json;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GetNewCustomerData {
	public static GetNewCustomerData get(String filename) throws JsonParseException, JsonMappingException, IOException  {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(new File(filename), GetNewCustomerData.class);
	}
	
	@JsonProperty ("customerName")
	String customerName;
	
	@JsonProperty ("gender")
	String gender;
	
	@JsonProperty ("DOB")
	String DOB;
	
	@JsonProperty ("address")
	String address;
	
	@JsonProperty ("city")
	String city;
	
	@JsonProperty ("state")
	String state;
	
	@JsonProperty ("phone")
	String phone;	

	@JsonProperty ("pin")
	String pin;
	
	@JsonProperty ("refix_email")
	String refix_email;	
	
	@JsonProperty ("password")
	String password;
	
	public String getCustomerName() {
		return customerName;
	}
	
	public String getGender() {
		return gender;
	}
	
	public String getDOB() {
		return DOB;
	}
	
	public String getAddress() {
		return address;
	}
	
	public String getCity() {
		return city;
	}
	
	public String getState() {
		return state;
	}
	
	public String getPin() {
		return pin;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public String getRefix_Email() {
		return refix_email;
	}
	
	public String getPassword() {
		return password;
	}
}
