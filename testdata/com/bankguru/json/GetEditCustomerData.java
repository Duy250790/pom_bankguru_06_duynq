package com.bankguru.json;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GetEditCustomerData {
	public static GetEditCustomerData get(String filename) throws JsonParseException, JsonMappingException, IOException  {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(new File(filename), GetEditCustomerData.class);
	}

	@JsonProperty ("address")
	String address;
	
	@JsonProperty ("city")
	String city;
	
	@JsonProperty ("state")
	String state;
	
	@JsonProperty ("phone")
	String phone;	

	@JsonProperty ("pin")
	String pin;
	
	@JsonProperty ("refix_email")
	String refix_email;
	
	public String getAddress() {
		return address;
	}
	
	public String getCity() {
		return city;
	}
	
	public String getState() {
		return state;
	}
	
	public String getPin() {
		return pin;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public String getRefix_Email() {
		return refix_email;
	}
	
	
}
