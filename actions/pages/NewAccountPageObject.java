package pages;

import org.openqa.selenium.WebDriver;

import bankguru.HomePageUI;
import bankguru.LoginPageUI;
import bankguru.NewAccountPageUI;
import bankguru.NewCustomerPageUI;
import bankguru.RegisterPageUI;
import commons.AbstractPage;
import commons.Constants;
public class NewAccountPageObject extends AbstractPage {
	private WebDriver driver;
	public NewAccountPageObject(WebDriver driver) {		
		this.driver = driver;
	}
	
	public void inputInforNewAccount(String customerID, String accountType, String initialDeposit) {
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"cusid");
		senkeyToElement(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,customerID,"cusid");	
		
		selectItemInDropdown(driver, NewAccountPageUI.ACCOUNTTYPE_SELECT, accountType);
		
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"inideposit");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTBOX_BUTTON,initialDeposit,"inideposit");
	}	
	
	public void clickSubmitNewAccountButton() {
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"button2");
		clickToElement(driver, Constants.DYNAMIC_TEXTBOX_BUTTON, "button2");
	}
	
	public boolean isCreateAccountSuccessDisplay() {
		waitForControlVisible(driver, NewAccountPageUI.CREATENEWACCOUNTSUCCESS_TITLE);
		return isControlDisplay(driver, NewAccountPageUI.CREATENEWACCOUNTSUCCESS_TITLE);
	}
	
	public String getCurrentAmountText() {
		return getTextElement(driver, Constants.DYNAMIC_TEXTBOX_SEBLING,"Current Amount");
	}
	
	public String getAccountID() {
		return getTextElement(driver, Constants.DYNAMIC_TEXTBOX_SEBLING,"Account ID");
	}
}
