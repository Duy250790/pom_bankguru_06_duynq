package pages;

import org.openqa.selenium.WebDriver;

import bankguru.BalanceEnquiryPageUI;
import bankguru.DepositPageUI;
import commons.Constants;
import commons.AbstractPage;

public class BalanceEnquiryPageObject extends AbstractPage {
	private WebDriver driver;
	public BalanceEnquiryPageObject(WebDriver driver) {		
		this.driver = driver;
	}
	
	public void inputToAccountNoTextbox(String accountNo) {
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"accountno");
		senkeyToElement(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,accountNo,"accountno");
	}
	
	public void clickSubmitBalanceEnquiryButton() {
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"AccSubmit");
		clickToElement(driver, Constants.DYNAMIC_TEXTBOX_BUTTON, "AccSubmit");
	}
	
	public boolean isBalanceEnquirySuccessDisplay(String accountNo) {
		waitForControlVisible(driver, BalanceEnquiryPageUI.BALANCEENQUIRYSUCCESS_TITLE,accountNo);
		return isControlDisplay(driver, BalanceEnquiryPageUI.BALANCEENQUIRYSUCCESS_TITLE,accountNo);
	}
	
	public String getBalanceText() {
		return getTextElement(driver, Constants.DYNAMIC_TEXTBOX_SEBLING,"Balance");
	}
}
