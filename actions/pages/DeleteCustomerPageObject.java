package pages;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import bankguru.BalanceEnquiryPageUI;
import bankguru.DeleteAccountPageUI;
import bankguru.DeleteCustomerPageUI;
import bankguru.DepositPageUI;
import commons.Constants;
import commons.AbstractPage;
import commons.AbstractTest;

public class DeleteCustomerPageObject extends AbstractPage {
	private WebDriver driver;
	public DeleteCustomerPageObject(WebDriver driver) {		
		this.driver = driver;
	}
	
	public void clickSubmitDeleteCustomerButton(WebDriver driver) {
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"AccSubmit");
		clickToElement(driver, Constants.DYNAMIC_TEXTBOX_BUTTON, "AccSubmit");
		staticSleep(10);
		acceptAlert(driver);		
		staticSleep(10);
	}
	
	public String getTextAlertDeleteCustomer(WebDriver driver) {
		return getTextAlert(driver);
	}
	
	
	
	
	
}
