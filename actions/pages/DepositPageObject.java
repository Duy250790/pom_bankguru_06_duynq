package pages;

import org.openqa.selenium.WebDriver;

import bankguru.DepositPageUI;
import bankguru.NewAccountPageUI;
import commons.AbstractPage;
import commons.Constants;

public class DepositPageObject extends AbstractPage {
	private WebDriver driver;
	public DepositPageObject(WebDriver driver) {		
		this.driver = driver;
	}
	
	public void inputToAccountNoTextbox(String accountNo) {
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"accountno");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTBOX_BUTTON,accountNo,"accountno");
	}
	
	public void inputToAmountDepositTextbox(String amount) {
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"ammount");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTBOX_BUTTON,amount,"ammount");
	}
	
	public void inputToDescriptionTextbox(String description) {
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"desc");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTBOX_BUTTON,description,"desc");
	}
	
	public void clickSubmitDepositButton() {
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"AccSubmit");
		clickToElement(driver, Constants.DYNAMIC_TEXTBOX_BUTTON, "AccSubmit");
	}
	
	public boolean isDepositSuccessDisplay(String accountNo) {
		waitForControlVisible(driver, DepositPageUI.DEPOSITSUCCESS_TITLE,accountNo);
		return isControlDisplay(driver, DepositPageUI.DEPOSITSUCCESS_TITLE,accountNo);
	}
	
	public String getCurrentBalanceText() {
		return getTextElement(driver, Constants.DYNAMIC_TEXTBOX_SEBLING,"Current Balance");
	}
}
