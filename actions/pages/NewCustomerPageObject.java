package pages;

import org.openqa.selenium.WebDriver;

import bankguru.NewCustomerPageUI;
import commons.Constants;
import commons.AbstractPage;
import commons.AbstractTest;

public class NewCustomerPageObject extends AbstractPage {
	private WebDriver driver;	
	public NewCustomerPageObject(WebDriver driver) {		
		this.driver = driver;
	}
	
	public void inputInfoNewCustomer(String customerName,String gender, String dob, String address, String city, String state, String pin, String phone, String email, String password) {
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"name");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTBOX_BUTTON,customerName,"name");
		
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"rad1");		
		if(gender == "male") {
			clickToElement(driver, Constants.DYNAMIC_TEXTBOX_BUTTON, "rad1");
		}else if (gender == "female") {
			clickToElement(driver, Constants.DYNAMIC_TEXTBOX_BUTTON, "rad2");
		}
		
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"dob");
		sendkeyToElementByJS(driver,Constants.DYNAMIC_TEXTBOX_BUTTON,dob,"dob");
		
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTAREA,"addr");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTAREA,address,"addr");
		
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"city");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTBOX_BUTTON,city,"city");
		
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"state");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTBOX_BUTTON,state,"state");
		
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"pinno");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTBOX_BUTTON,pin,"pinno");
		
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"telephoneno");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTBOX_BUTTON,phone,"telephoneno");
		
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"emailid");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTBOX_BUTTON,email,"emailid");		
		
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"password");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTBOX_BUTTON,password,"password");
	}
	
	public void clickSubmitButton() {
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"sub");
		clickToElement(driver, Constants.DYNAMIC_TEXTBOX_BUTTON, "sub");
	}
	
	public boolean isCreateCustomerSuccessDisplay() {
		waitForControlVisible(driver, NewCustomerPageUI.CREATENEWCUSOMERSUCCESS_TITLE);
		return isControlDisplay(driver, NewCustomerPageUI.CREATENEWCUSOMERSUCCESS_TITLE);
	}
	
	public String getCustomerIDText() {
		return getTextElement(driver, Constants.DYNAMIC_TEXTBOX_SEBLING,"Customer ID");
	}
	
	public void verifyInfoNewCustomerCreatedSucess(String customerName,String gender, String dob, String address, String city, String state, String pin, String phone, String email) {
		verifyEqual(getTextElement(driver, Constants.DYNAMIC_TEXTBOX_SEBLING,"Customer Name"), customerName);
		verifyEqual(getTextElement(driver, Constants.DYNAMIC_TEXTBOX_SEBLING,"Gender"), gender);
		
		String dob1=dob.split("/")[2]+"-"+dob.split("/")[0]+"-"+dob.split("/")[1];	
		verifyEqual(getTextElement(driver, Constants.DYNAMIC_TEXTBOX_SEBLING,"Birthdate"), dob1);
		
		verifyEqual(getTextElement(driver, Constants.DYNAMIC_TEXTBOX_SEBLING,"Address"), address);
		verifyEqual(getTextElement(driver, Constants.DYNAMIC_TEXTBOX_SEBLING,"City"), city);
		verifyEqual(getTextElement(driver, Constants.DYNAMIC_TEXTBOX_SEBLING,"State"), state);
		verifyEqual(getTextElement(driver, Constants.DYNAMIC_TEXTBOX_SEBLING,"Pin"), pin);
		verifyEqual(getTextElement(driver, Constants.DYNAMIC_TEXTBOX_SEBLING,"Mobile No."), phone);
		verifyEqual(getTextElement(driver, Constants.DYNAMIC_TEXTBOX_SEBLING,"Email"), email);		
	}

}
