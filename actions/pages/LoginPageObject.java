package pages;

import org.openqa.selenium.WebDriver;

import bankguru.HomePageUI;
import bankguru.LoginPageUI;
import commons.AbstractPage;

public class LoginPageObject extends AbstractPage {
	private WebDriver driver;
	public LoginPageObject(WebDriver driver) {
		this.driver = driver;
	}
	
	public String getLoginPageURL() {
		return getCurrentURL(driver);		
	}
	
	public void inputToUserIDTextbox(String username) {
		waitForControlVisible(driver, LoginPageUI.DYNAMIC_TEXTBOX_BUTTON,"uid");
		senkeyToElement(driver,LoginPageUI.DYNAMIC_TEXTBOX_BUTTON, username,"uid");
	}
	
	public void inputToPasswordTextbox(String password) {
		waitForControlVisible(driver, LoginPageUI.DYNAMIC_TEXTBOX_BUTTON,"password");
		senkeyToElement(driver,LoginPageUI.DYNAMIC_TEXTBOX_BUTTON, password,"password");
	}
	
	public HomePageObject clickToLoginButton() {
		waitForControlVisible(driver, LoginPageUI.DYNAMIC_TEXTBOX_BUTTON,"btnLogin");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			clickToElementByJS(driver, LoginPageUI.DYNAMIC_TEXTBOX_BUTTON,"btnLogin");
			staticSleep(5);
		}else {		
			clickToElement(driver, LoginPageUI.DYNAMIC_TEXTBOX_BUTTON,"btnLogin");	
		}
			
		return new HomePageObject(driver);
	}
	
	public RegisterPageObject clickToHereLink() {
		waitForControlVisible(driver, LoginPageUI.DYNAMIC_PAGE_LINK,"here");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			clickToElementByJS(driver, LoginPageUI.DYNAMIC_PAGE_LINK,"here");
			staticSleep(5);
		}else {		
			clickToElement(driver, LoginPageUI.DYNAMIC_PAGE_LINK,"here");
		}
		return new RegisterPageObject(driver);
	}
	public boolean isLoginFormDisplay() {
		waitForControlVisible(driver, LoginPageUI.LOGIN_FORM);
		return isControlDisplay(driver, LoginPageUI.LOGIN_FORM);
	}
	
	public boolean isNotLoginFormDisplay() {
		waitForControlInVisible(driver, LoginPageUI.LOGIN_FORM);
		return isControlNotDisplay(driver, LoginPageUI.LOGIN_FORM);
	}
}
