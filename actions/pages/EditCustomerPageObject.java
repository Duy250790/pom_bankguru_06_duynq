package pages;

import org.openqa.selenium.WebDriver;

import commons.Constants;
import bankguru.EditCustomerPageUI;
import commons.AbstractPage;

public class EditCustomerPageObject extends AbstractPage {
	private WebDriver driver;
	public EditCustomerPageObject(WebDriver driver) {		
		this.driver = driver;
	}
	
//	public void inputToCustomerIDTextbox(String customerID) {
//		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"cusid");
//		senkeyToElement(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,customerID,"cusid");
//	}
	
	public void clickSubmitButton() {
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"AccSubmit");
		clickToElement(driver, Constants.DYNAMIC_TEXTBOX_BUTTON, "AccSubmit");
	}
	
	public void inputInfoEditCustomer( String address, String city, String state, String pin, String phone, String email) {
	
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTAREA,"addr");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTAREA,address,"addr");
		
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"city");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTBOX_BUTTON,city,"city");
		
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"state");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTBOX_BUTTON,state,"state");
		
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"pinno");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTBOX_BUTTON,pin,"pinno");
		
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"telephoneno");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTBOX_BUTTON,phone,"telephoneno");
		
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"emailid");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTBOX_BUTTON,email,"emailid");		
	}	
	
	public void clickSubmitEditButton() {
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"sub");
		clickToElement(driver, Constants.DYNAMIC_TEXTBOX_BUTTON, "sub");
	}
	
	public boolean isUpdateCustomerSuccessDisplay() {
		waitForControlVisible(driver, EditCustomerPageUI.EDITCUSOMERSUCCESS_TITLE);
		return isControlDisplay(driver, EditCustomerPageUI.EDITCUSOMERSUCCESS_TITLE);
	}
	
	public void verifyInfoEditCustomerUpdatedSucess( String address, String city, String state, String pin, String phone, String email) {		
		verifyEqual(getTextElement(driver, Constants.DYNAMIC_TEXTBOX_SEBLING,"Address"), address);
		verifyEqual(getTextElement(driver, Constants.DYNAMIC_TEXTBOX_SEBLING,"City"), city);
		verifyEqual(getTextElement(driver, Constants.DYNAMIC_TEXTBOX_SEBLING,"State"), state);
		verifyEqual(getTextElement(driver, Constants.DYNAMIC_TEXTBOX_SEBLING,"Pin"), pin);
		verifyEqual(getTextElement(driver, Constants.DYNAMIC_TEXTBOX_SEBLING,"Mobile No."), phone);
		verifyEqual(getTextElement(driver, Constants.DYNAMIC_TEXTBOX_SEBLING,"Email"), email);		
	}
	
}
