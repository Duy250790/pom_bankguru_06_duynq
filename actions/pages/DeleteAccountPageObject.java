package pages;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import bankguru.BalanceEnquiryPageUI;
import bankguru.DeleteAccountPageUI;
import bankguru.DepositPageUI;
import commons.Constants;
import commons.AbstractPage;

public class DeleteAccountPageObject extends AbstractPage {
	private WebDriver driver;
	public DeleteAccountPageObject(WebDriver driver) {		
		this.driver = driver;
	}
	
	public void clickSubmitDeleteAccountButton(WebDriver driver) {
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"AccSubmit");
		clickToElement(driver, Constants.DYNAMIC_TEXTBOX_BUTTON, "AccSubmit");
		staticSleep(10);
		acceptAlert(driver);
		staticSleep(10);
	}
	
	public String getTextAlertDeleteAccount(WebDriver driver) {
		return getTextAlert(driver);
	}
	
	
	
	
	
}
