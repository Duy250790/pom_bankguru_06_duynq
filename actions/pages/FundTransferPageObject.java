package pages;

import org.openqa.selenium.WebDriver;

import bankguru.FundTransferPageUI;
import bankguru.WithdrawalPageUI;
import commons.AbstractPage;
import commons.Constants;

public class FundTransferPageObject extends AbstractPage {
	private WebDriver driver;
	public FundTransferPageObject(WebDriver driver) {		
		this.driver = driver;
	}
	public void inputToPayersAccountNoTextbox(String payersAccountNo) {
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"payersaccount");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTBOX_BUTTON,payersAccountNo,"payersaccount");
	}
	
	public void inputToPayeesAccountNoTextbox(String payeesAccountNo) {
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"payeeaccount");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTBOX_BUTTON,payeesAccountNo,"payeeaccount");
	}
	
	public void inputToAmountTransferTextbox(String amount) {
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"ammount");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTBOX_BUTTON,amount,"ammount");
	}
	
	public void inputToDescriptionTextbox(String description) {
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"desc");
		senkeyToElement(driver,Constants.DYNAMIC_TEXTBOX_BUTTON,description,"desc");
	}
	
	public void clickSubmitTransferButton() {
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"AccSubmit");
		clickToElement(driver, Constants.DYNAMIC_TEXTBOX_BUTTON, "AccSubmit");
	}
	
	public boolean isFundTransferSuccessDisplay() {
		waitForControlVisible(driver, FundTransferPageUI.TRANSFERSUCCESS_TITLE);
		return isControlDisplay(driver, FundTransferPageUI.TRANSFERSUCCESS_TITLE);
	}
	
	public String getAmountTranferText() {
		return getTextElement(driver, Constants.DYNAMIC_TEXTBOX_SEBLING,"Amount");
	}
}
