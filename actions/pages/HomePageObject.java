package pages;

import org.openqa.selenium.WebDriver;

import bankguru.HomePageUI;
import commons.AbstractPage;

public class HomePageObject extends AbstractPage {
	private WebDriver driver;
	public HomePageObject(WebDriver driver) {		
		this.driver = driver;
	}
	public boolean isHomePageDisplay() {
		waitForControlVisible(driver, HomePageUI.HOMEPAGE_TITLE);
		return isControlDisplay(driver, HomePageUI.HOMEPAGE_TITLE);
	}		
	
}
