package pages;

import org.openqa.selenium.WebDriver;

import bankguru.LoginPageUI;
import bankguru.RegisterPageUI;
import commons.AbstractPage;

public class RegisterPageObject extends AbstractPage {
	private WebDriver driver;
	public RegisterPageObject(WebDriver driver) {
		this.driver = driver;
	}
	
	public void inputToEmailTextbox(String email) {
		waitForControlVisible(driver, RegisterPageUI.DYNAMIC_TEXTBOX_BUTTON,"emailid");
		senkeyToElement(driver, RegisterPageUI.DYNAMIC_TEXTBOX_BUTTON,email,"emailid");
	}
	
	public void clickSubmitButton() {
		waitForControlVisible(driver, RegisterPageUI.DYNAMIC_TEXTBOX_BUTTON,"btnLogin");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			clickToElementByJS(driver, RegisterPageUI.DYNAMIC_TEXTBOX_BUTTON,"btnLogin");
			staticSleep(5);
		}else {		
			clickToElement(driver, RegisterPageUI.DYNAMIC_TEXTBOX_BUTTON,"btnLogin");
		}		
	}
	
	public String getUserIDText() {
		return getTextElement(driver, RegisterPageUI.DYNAMIC_TEXTBOX_SEBLING,"User ID :");
	}
	
	public String getPasswordText() {
		return getTextElement(driver, RegisterPageUI.DYNAMIC_TEXTBOX_SEBLING,"Password :");
	}
	
	public LoginPageObject openLoginPage(String url) {
		openAnyURL(driver, url);
		if(driver.toString().toLowerCase().contains("internetexplorer")){
			staticSleep(5);
		}
		return new LoginPageObject(driver);
	}
}
