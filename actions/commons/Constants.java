package commons;

public class Constants {
	public static final String DEV_URL ="http://demo.guru99.com/v4/";
	public static final String TEST_URL ="http://test.guru99.com/v4/";
	public static final String PROJECT_NAME_HTML= "BankGuruReport.html";
	public static final String USERID= "mngr170039";
	public static final String PASSWORD= "EhUsEvU";
	public static final String DYNAMIC_TEXTBOX_BUTTON="//input[@name='%s']";
	public static final String DYNAMIC_TEXTAREA="//textarea[@name='%s']";
	public static final String DYNAMIC_TEXTBOX_SEBLING="//td[text()='%s']/following-sibling::td";
}
