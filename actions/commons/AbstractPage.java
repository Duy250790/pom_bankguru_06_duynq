package commons;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import bankguru.AbstractPageUI;
import bankguru.DepositPageUI;
import bankguru.EditCustomerPageUI;
import bankguru.FundTransferPageUI;
import bankguru.HomePageUI;
import bankguru.LoginPageUI;
import bankguru.NewCustomerPageUI;
import pages.BalanceEnquiryPageObject;
import pages.DeleteAccountPageObject;
import pages.DeleteCustomerPageObject;
import pages.DepositPageObject;
import pages.EditCustomerPageObject;
import pages.FundTransferPageObject;
import pages.LoginPageObject;
import pages.NewAccountPageObject;
import pages.NewCustomerPageObject;
import pages.WithdrawalPageObject;

public class AbstractPage extends AbstractTest {
	
	public void openAnyURL(WebDriver driver, String URL) {
		driver.get(URL);			
		
	}
	public String getTitle(WebDriver driver) {
		return driver.getTitle();
	}

	public String getCurrentURL(WebDriver driver) {			
		return driver.getCurrentUrl();		
	}

	public String getPageSource(WebDriver driver) {
		return driver.getPageSource();
	}

	public void back(WebDriver driver) {
		driver.navigate().back();
	}

	public void forward(WebDriver driver) {
		driver.navigate().forward();
	}

	public void refresh(WebDriver driver) {
		driver.navigate().refresh();
	}

	public void acceptAlert(WebDriver driver) {
		driver.switchTo().alert().accept();
	}

	public void cancelAlert(WebDriver driver) {
		driver.switchTo().alert().dismiss();
	}

	public String getTextAlert(WebDriver driver) {
		return driver.switchTo().alert().getText();
	}
	
	

	public void sendkeyAlert(WebDriver driver, String text) {
		driver.switchTo().alert().sendKeys(text);
	}
	public void staticSleep(long timeout) {
		try {
			Thread.sleep(timeout*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void clickToElement(WebDriver driver, String xpathExpression) {
		WebElement element = driver.findElement(By.xpath(xpathExpression));
		element.click();
		if(driver.toString().toLowerCase().contains("internetexplorer")){
			staticSleep(5);
		}
	}
	
	public void clickToElement(WebDriver driver, String xpathExpression, String... values) {
		xpathExpression=String.format(xpathExpression, (Object[]) values);
		WebElement element = driver.findElement(By.xpath(xpathExpression));
		element.click();
		if(driver.toString().toLowerCase().contains("internetexplorer")){
			staticSleep(5);
		}
	}
	
	
	public void senkeyToElement(WebDriver driver, String xpathExpression, String value) {
		WebElement element = driver.findElement(By.xpath(xpathExpression));
		element.clear();
		element.sendKeys(value);
	}
	
	public void senkeyToElement(WebDriver driver, String xpathExpression ,String valueItem,String... values) {
		xpathExpression=String.format(xpathExpression, (Object[]) values);
		WebElement element = driver.findElement(By.xpath(xpathExpression));
		element.clear();
		element.sendKeys(valueItem);
	}

	public void selectItemInDropdown(WebDriver driver, String xpathExpression, String valueItem) {
		WebElement element = driver.findElement(By.xpath(xpathExpression));
		Select select = new Select(element);
		select.selectByVisibleText(valueItem);
	}

	public String getSelectedItemInHtmlDropdown(WebDriver driver, String xpathExpression) {
		WebElement element = driver.findElement(By.xpath(xpathExpression));
		Select select = new Select(element);
		return select.getFirstSelectedOption().getText();
	}

	public String getAttributeValue(WebDriver driver, String xpathExpression, String attributeName) {
		WebElement element = driver.findElement(By.xpath(xpathExpression));
		return element.getAttribute(attributeName);
	}

	public String getTextElement(WebDriver driver, String xpathExpression) {
		WebElement element = driver.findElement(By.xpath(xpathExpression));
		return element.getText();
	}
	
	public String getTextElement(WebDriver driver, String xpathExpression, String... values) {
		xpathExpression = String.format(xpathExpression, (Object[]) values);
		WebElement element = driver.findElement(By.xpath(xpathExpression));
		return element.getText();
	}

	public int countElementNumber(WebDriver driver, String xpathExpression) {
		List<WebElement> element = driver.findElements(By.xpath(xpathExpression));
		return element.size();
	}

	public void checkTheCheckbox(WebDriver driver, String xpathExpression) {
		WebElement element = driver.findElement(By.xpath(xpathExpression));
		if (!element.isSelected()) {
			element.click();
		}
	}

	public void uncheckTheCheckbox(WebDriver driver, String xpathExpression) {
		WebElement element = driver.findElement(By.xpath(xpathExpression));
		if (element.isSelected()) {
			element.click();
		}
	}
	
	public boolean isControlDisplay(WebDriver driver, String xpathExpression,String... values) {
		xpathExpression=String.format(xpathExpression, (Object[]) values);
		WebElement element = driver.findElement(By.xpath(xpathExpression));
		return element.isDisplayed();
	}
	
	public boolean isControlDisplay(WebDriver driver, String xpathExpression) {
		WebElement element = driver.findElement(By.xpath(xpathExpression));
		return element.isDisplayed();
	}
	
	public boolean isControlNotDisplay(WebDriver driver, String xpathExpression) {
		Date date = new Date();
		System.out.println("Thoi gian Start:" + date);
		overrideGlobalTimeOut(driver, shortTimeOut);
		List <WebElement> element = driver.findElements(By.xpath(xpathExpression));
		if(element.size()==0) {
			date = new Date();
			System.out.println("Thoi gian End:" + date);
			overrideGlobalTimeOut(driver, longTimeOut);
			return true;
		}
		else {
			System.out.println("Thoi gian End:" + date);
			overrideGlobalTimeOut(driver, longTimeOut);
			return false;
			
			}		
	}
	
	public void overrideGlobalTimeOut(WebDriver driver, long timeout) {
		driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.MILLISECONDS);
	}
	
	public boolean isControlSelected(WebDriver driver, String xpathExpression) {
		WebElement element = driver.findElement(By.xpath(xpathExpression));
		return element.isEnabled();
	}
	
	public boolean isControlEnabled(WebDriver driver, String xpathExpression) {
		WebElement element = driver.findElement(By.xpath(xpathExpression));
		return element.isEnabled();
	}
	
	public void switchToWindowByID(WebDriver driver,String parent) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindow : allWindows) {
			if (!runWindow.equals(parent)) {
				driver.switchTo().window(runWindow);
				break;
			}
		}
	}

	public void switchToWindowByTitle(WebDriver driver,String title) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindows : allWindows) {
			driver.switchTo().window(runWindows);
			String currentWin = driver.getTitle();
			if (currentWin.equals(title)) {
				break;
			}
		}
	}

	public boolean closeAllWithoutParentWindows(WebDriver driver,String parentWindow) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindows : allWindows) {
			if (!runWindows.equals(parentWindow)) {
				driver.switchTo().window(runWindows);
				driver.close();
			}
		}
		driver.switchTo().window(parentWindow);
		if (driver.getWindowHandles().size() == 1)
			return true;
		else
			return false;
	}
	
	public String getJsAlertText(WebDriver driver)
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		Object txt= js.executeScript("return window.alert.myAlertText;");
		return (String)txt;
	}
	
	public void highlightElement(WebDriver driver, WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].style.border='6px groove red'", element);
	}

	public Object executeForBrowser(WebDriver driver,String javaSript) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript(javaSript);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object clickToElementByJS(WebDriver driver,String locator) {
		try {
			WebElement element = driver.findElement(By.xpath(locator));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}
	public Object clickToElementByJS(WebDriver driver,String xpathExpression, String... values) {
		try {
			xpathExpression=String.format(xpathExpression, (Object[]) values);
			WebElement element = driver.findElement(By.xpath(xpathExpression));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object sendkeyToElementByJS(WebDriver driver,String xpathExpression,String valueItems, String... values) {
		try {
			xpathExpression=String.format(xpathExpression, (Object[]) values);
			WebElement element = driver.findElement(By.xpath(xpathExpression));
			element.clear();
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].setAttribute('value', '" + valueItems + "')", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}
	
	

	public Object removeAttributeInDOM(WebDriver driver,String locator, String attribute) {
		try {
			WebElement element = driver.findElement(By.xpath(locator));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].removeAttribute('" + attribute + "');", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object scrollToBottomPage(WebDriver driver) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}
	
	public Object scrollToElement(WebDriver driver, String locator) {
		try {
			WebElement element = driver.findElement(By.xpath(locator));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].scrollIntoView(true);", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}
	
	public void waitForControlVisible(WebDriver driver, String xpathExpression) {		
		By by = By.xpath(xpathExpression);
		WebDriverWait wait = new WebDriverWait(driver,longTimeOut);		
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));		
	}
	
	public void waitForControlVisible(WebDriver driver, String xpathExpression, String... values) {	
		xpathExpression=String.format(xpathExpression, (Object[]) values);
		By by = By.xpath(xpathExpression);
		WebDriverWait wait = new WebDriverWait(driver,longTimeOut);		
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));		
	}
	
	public void waitForControlInVisible(WebDriver driver, String xpathExpression) {		
		By by = By.xpath(xpathExpression);
		WebDriverWait wait = new WebDriverWait(driver,longTimeOut);
		overrideGlobalTimeOut(driver, shortTimeOut);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
		overrideGlobalTimeOut(driver, longTimeOut);
		
	}
	
	public NewCustomerPageObject openNewCustomerLink(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"New Customer");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			clickToElementByJS(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"New Customer");
			staticSleep(5);
		}else {		
			clickToElement(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"New Customer");	
		}		
		return PageFactoryManage.getNewCustomerPage(driver);
	}	
	
	public EditCustomerPageObject openEditCustomerLink(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Edit Customer");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			clickToElementByJS(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Edit Customer");
			staticSleep(5);
		}else {		
			clickToElement(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Edit Customer");	
		}		
		return PageFactoryManage.getEditCustomerPage(driver);
	}
	
	public NewAccountPageObject openNewAccountLink(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"New Account");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			clickToElementByJS(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"New Account");
			staticSleep(5);
		}else {		
			clickToElement(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"New Account");	
		}		
		return PageFactoryManage.getNewAccountPage(driver);
	}
	
	public WithdrawalPageObject openWithdrawalLink(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Withdrawal");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			clickToElementByJS(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Withdrawal");
			staticSleep(5);
		}else {		
			clickToElement(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Withdrawal");	
		}		
		return PageFactoryManage.getWithdrawalPage(driver);
	}
	
	public DepositPageObject openDepositCustomerLink(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Deposit");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			clickToElementByJS(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Deposit");
			staticSleep(5);
		}else {		
			clickToElement(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Deposit");	
		}
		return PageFactoryManage.getDepositPage(driver);
	}
	
	public FundTransferPageObject openFundTransferLink(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Fund Transfer");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			clickToElementByJS(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Fund Transfer");
			staticSleep(5);
		}else {		
			clickToElement(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Fund Transfer");	
		}		
		return PageFactoryManage.getFunTransferPage(driver);
	}
	
	public BalanceEnquiryPageObject openBalanceEnquiryLink(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Balance Enquiry");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			clickToElementByJS(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Balance Enquiry");
			staticSleep(5);
		}else {		
			clickToElement(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Balance Enquiry");	
		}		
		return PageFactoryManage.getBalanceEnquiryPage(driver);
	}
	
	public DeleteAccountPageObject openDeleteAccountLink(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Delete Account");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			clickToElementByJS(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Delete Account");
			staticSleep(5);
		}else {		
			clickToElement(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Delete Account");	
		}		
		return PageFactoryManage.getDeleteAccountPage(driver);
	}	
	
	public DeleteCustomerPageObject openDeleteCustomerLink(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Delete Customer");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			clickToElementByJS(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Delete Customer");
			staticSleep(5);
		}else {		
			clickToElement(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Delete Customer");	
		}		
		return PageFactoryManage.getDeleteCustomerPage(driver);
	}	
	
	public LoginPageObject clickToLogoutLink(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Log out");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			clickToElementByJS(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Log out");
			staticSleep(5);
		}else {		
			clickToElement(driver, AbstractPageUI.DYNAMIC_PAGE_LINK,"Log out");	
		}	
		acceptAlert(driver);
		return PageFactoryManage.getLoginPage(driver);
	}
	
	public void inputToCustomerIDTextbox(WebDriver driver,String customerID) {
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"cusid");
		senkeyToElement(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,customerID,"cusid");
	}
	
	public void inputToAccountNoTextbox(WebDriver driver,String accountNo) {
		waitForControlVisible(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,"accountno");
		senkeyToElement(driver, Constants.DYNAMIC_TEXTBOX_BUTTON,accountNo,"accountno");
	}	
	
	private long shortTimeOut = 5;	
	private long longTimeOut = 30;	
}
