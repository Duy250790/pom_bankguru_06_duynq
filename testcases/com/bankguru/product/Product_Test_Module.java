package com.bankguru.product;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.bankguru.common.Common_01_RegisterToSystem;

import commons.AbstractPage;
import commons.AbstractTest;
import commons.Constants;
import commons.PageFactoryManage;
import pages.DepositPageObject;
import pages.EditCustomerPageObject;
import pages.FundTransferPageObject;
import pages.HomePageObject;
import pages.LoginPageObject;
import pages.NewCustomerPageObject;
import pages.RegisterPageObject;



public class Product_Test_Module extends AbstractTest {
	private WebDriver driver;
	private String userID,password,loginPageUrl;
	private pages.RegisterPageObject registerPage;
	private pages.LoginPageObject loginPage;
	private pages.HomePageObject homePage;
	
	
	@Parameters("browser")
	@BeforeClass
	public void beforeClass(String BrowserName)  {		
		driver=OpenMultiBrowser(BrowserName);
		loginPage = PageFactoryManage.getLoginPage(driver);	
		System.out.println("User ID at Payment : " + Common_01_RegisterToSystem.userID);
		System.out.println("PassWord at Payment: " + Common_01_RegisterToSystem.password);
	}

	
	
	@Test
	public void TC_02_LoginToSystem() {			
		verifyTrue(loginPage.isLoginFormDisplay());
		loginPage.inputToUserIDTextbox(Common_01_RegisterToSystem.userID);
		loginPage.inputToPasswordTextbox(Common_01_RegisterToSystem.password);
		homePage=loginPage.clickToLoginButton();
		verifyTrue(homePage.isHomePageDisplay());
	}

	@AfterClass
	public void afterClass() {
		quitBrowser(driver);
	}
	
	

}
