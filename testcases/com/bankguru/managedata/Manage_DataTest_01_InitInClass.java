package com.bankguru.managedata;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import commons.AbstractPage;
import commons.AbstractTest;
import commons.Constants;
import commons.PageFactoryManage;
import pages.DepositPageObject;
import pages.EditCustomerPageObject;
import pages.FundTransferPageObject;
import pages.HomePageObject;
import pages.LoginPageObject;
import pages.NewCustomerPageObject;
import pages.RegisterPageObject;



public class Manage_DataTest_01_InitInClass extends AbstractTest {
	private WebDriver driver;
	private String email;	
	
	@Parameters("browser")
	@BeforeClass
	public void beforeClass(String BrowserName)  {		
		driver=OpenMultiBrowser(BrowserName);
		email = "kdc"+randomNumber()+"@gmail.com";
	}

	
	
	@Test
	public void TC_01_LoginToSystem() {	
		System.out.println("Name = " + com.bankguru.data.Customer.CUSTOMERNAME);
		System.out.println("Gender = " + com.bankguru.data.Customer.GENDER);
		System.out.println("Date of Birthday = " + com.bankguru.data.Customer.DATE_OF_BIRTH);
		System.out.println("Address = " + com.bankguru.data.Customer.ADDRESS);
		System.out.println("City = " + com.bankguru.data.Customer.CITY);
		System.out.println("State = " + com.bankguru.data.Customer.STATE);
		System.out.println("Email = " + com.bankguru.data.Customer.PREFIX_EMAIL+email);
		System.out.println("Pass = " + com.bankguru.data.Customer.PASSWORD);
	}
	


	@AfterClass
	public void afterClass() {
		quitBrowser(driver);
	}
	
	

}
