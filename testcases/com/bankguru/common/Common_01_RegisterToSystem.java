package com.bankguru.common;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import commons.AbstractPage;
import commons.AbstractTest;
import commons.Constants;
import commons.PageFactoryManage;
import pages.DepositPageObject;
import pages.EditCustomerPageObject;
import pages.FundTransferPageObject;
import pages.HomePageObject;
import pages.LoginPageObject;
import pages.NewCustomerPageObject;
import pages.RegisterPageObject;



public class Common_01_RegisterToSystem extends AbstractTest {
	private WebDriver driver;
	private String email;
	public static String userID,password;
	private pages.RegisterPageObject registerPage;
	private pages.LoginPageObject loginPage;	
	
	
	@Parameters("browser")
	@BeforeSuite
	public void beforeSuite(String BrowserName)  {		
		driver=OpenMultiBrowser(BrowserName);
		loginPage = PageFactoryManage.getLoginPage(driver);
		email = "kdc"+randomNumber()+"@gmail.com";		
		registerPage=loginPage.clickToHereLink();
		registerPage.inputToEmailTextbox(email);
		registerPage.clickSubmitButton();
		userID=registerPage.getUserIDText();
		password=registerPage.getPasswordText();
		
		System.out.println("User ID at Common : " + userID);
		System.out.println("PassWord at Comon: " + password);
		
		log.info("CommonUser : Step 05 - Close Browser");
		quitBrowser(driver);
	}

	
	
	

}
