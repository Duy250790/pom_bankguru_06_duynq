package com.bankguru.payment;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.bankguru.json.GetEditCustomerData;
import com.bankguru.json.GetNewCustomerData;
import com.bankguru.json.GetPaymentData;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import bankguru.DeleteAccountPageUI;
import bankguru.DeleteCustomerPageUI;
import commons.AbstractTest;
import commons.Constants;
import commons.PageFactoryManage;
import javafx.scene.control.Alert;
import pages.BalanceEnquiryPageObject;
import pages.DeleteAccountPageObject;
import pages.DeleteCustomerPageObject;
import pages.DepositPageObject;
import pages.EditCustomerPageObject;
import pages.FundTransferPageObject;
import pages.HomePageObject;
import pages.LoginPageObject;
import pages.NewAccountPageObject;
import pages.NewCustomerPageObject;
import pages.WithdrawalPageObject;



public class Payment_Function extends AbstractTest {
	WebDriver driver;
	private String email,customerID,accountID,currentAmount;
	private float totalAmount; 
	
	private LoginPageObject loginPage;
	private PageFactoryManage pageFactoryManage;
	private HomePageObject homePage;
	private NewCustomerPageObject newCustomerPage;
	private EditCustomerPageObject editCustomerPage;
	private NewAccountPageObject newAccountPage;
	private DepositPageObject depositPage;
	private WithdrawalPageObject withdrawalPage;
	private FundTransferPageObject fundTransferPage;
	private BalanceEnquiryPageObject balanceEnquiryPage;
	private DeleteAccountPageObject deleteAccountPage;
	private DeleteCustomerPageObject deleteCustomerPage;
	
	private GetNewCustomerData newCustomerData;
	private GetEditCustomerData editCusomerData;
	private GetPaymentData paymentData;
	
	
	@Parameters({"browser","newCustomer","updateCustomer","payment"})
	@BeforeClass
	public void beforeClass(String browsername, String newCustomer, String editCusomer, String payment) throws JsonParseException, JsonMappingException, IOException  {		
		driver=OpenMultiBrowser(browsername);
		newCustomerData = new GetNewCustomerData().get(newCustomer);
		editCusomerData= new GetEditCustomerData().get(editCusomer);
		paymentData = new GetPaymentData().get(payment);
		
		loginPage = pageFactoryManage.getLoginPage(driver);
		loginPage.inputToUserIDTextbox(Constants.USERID);
		loginPage.inputToPasswordTextbox(Constants.PASSWORD);
		homePage=loginPage.clickToLoginButton();
		email = randomNumber()+"@gmail.com";	
		customerID="";
		accountID="";
		currentAmount="";
		totalAmount=0;
		
	}

	@Test
	public void TC_01_CreateNewCustomerAccount() {			
		newCustomerPage = homePage.openNewCustomerLink(driver);
		newCustomerPage.inputInfoNewCustomer(newCustomerData.getCustomerName(), newCustomerData.getGender(), newCustomerData.getDOB(), newCustomerData.getAddress(), newCustomerData.getCity(), newCustomerData.getState(), newCustomerData.getPin(), newCustomerData.getPhone(), newCustomerData.getRefix_Email()+email, newCustomerData.getPassword());
		newCustomerPage.clickSubmitButton();
		verifyTrue(newCustomerPage.isCreateCustomerSuccessDisplay());	
		newCustomerPage.verifyInfoNewCustomerCreatedSucess(newCustomerData.getCustomerName(), newCustomerData.getGender(), newCustomerData.getDOB(), newCustomerData.getAddress(), newCustomerData.getCity(), newCustomerData.getState(), newCustomerData.getPin(), newCustomerData.getPhone(), newCustomerData.getRefix_Email()+email);
		customerID = newCustomerPage.getCustomerIDText();
	}
	
	@Test
	public void TC_02_EditCustomerAccount() {			
		editCustomerPage=newCustomerPage.openEditCustomerLink(driver);
		editCustomerPage.inputToCustomerIDTextbox(driver, customerID);
		editCustomerPage.clickSubmitButton();
		editCustomerPage.inputInfoEditCustomer(editCusomerData.getAddress(),editCusomerData.getCity(),editCusomerData.getState(),editCusomerData.getPin(),editCusomerData.getPhone(),editCusomerData.getRefix_Email()+email);
		editCustomerPage.clickSubmitEditButton();
		editCustomerPage.isUpdateCustomerSuccessDisplay();
		editCustomerPage.verifyInfoEditCustomerUpdatedSucess(editCusomerData.getAddress(),editCusomerData.getCity(),editCusomerData.getState(),editCusomerData.getPin(),editCusomerData.getPhone(),editCusomerData.getRefix_Email()+email);
	}
	
	@Test
	public void TC_03_CreateAccount() {			
		newAccountPage = editCustomerPage.openNewAccountLink(driver);
		newAccountPage.inputInforNewAccount(customerID, paymentData.getAccountType(), paymentData.getInitialDeposit());
		newAccountPage.clickSubmitNewAccountButton();
		newAccountPage.isCreateAccountSuccessDisplay();
		currentAmount =newAccountPage.getCurrentAmountText();
		accountID=newAccountPage.getAccountID();
		totalAmount=totalAmount + Float.parseFloat(currentAmount); 
		verifyEqual(currentAmount, paymentData.getInitialDeposit());
	}
	
	@Test
	public void TC_04_Deposit() {			
		depositPage=newAccountPage.openDepositCustomerLink(driver);
		depositPage.inputToAccountNoTextbox(accountID);
		depositPage.inputToAmountDepositTextbox(paymentData.getAmountDeoposit());
		depositPage.inputToDescriptionTextbox("Deposit");
		depositPage.clickSubmitDepositButton();
		depositPage.isDepositSuccessDisplay(accountID);
		currentAmount = depositPage.getCurrentBalanceText();
		totalAmount=totalAmount + Float.parseFloat(paymentData.getAmountDeoposit()); 		
		verifyEqual(totalAmount , Float.parseFloat(currentAmount));
	}
	
	@Test
	public void TC_05_Withdrawal() {			
		withdrawalPage=depositPage.openWithdrawalLink(driver);
		withdrawalPage.inputToAccountNoTextbox(accountID);
		withdrawalPage.inputToAmountWithdrawalTextbox(paymentData.getAmountWithdrawal());
		withdrawalPage.inputToDescriptionTextbox("Withdrawal");
		withdrawalPage.clickSubmitWithdrawalButton();
		withdrawalPage.isWithdrawalSuccessDisplay(accountID);
		currentAmount = depositPage.getCurrentBalanceText();
		totalAmount=totalAmount - Float.parseFloat(paymentData.getAmountWithdrawal()); 		
		verifyEqual(totalAmount , Float.parseFloat(currentAmount));
	}
	
	@Test
	public void TC_06_FundTransfer() {			
		fundTransferPage=withdrawalPage.openFundTransferLink(driver);
		fundTransferPage.inputToPayersAccountNoTextbox(accountID);
		fundTransferPage.inputToPayeesAccountNoTextbox(paymentData.getAccountPayee());
		fundTransferPage.inputToAmountTransferTextbox(paymentData.getAmountTransfer());
		fundTransferPage.inputToDescriptionTextbox("Transfer");
		fundTransferPage.clickSubmitTransferButton();
		String amount = fundTransferPage.getAmountTranferText();
		totalAmount=totalAmount - Float.parseFloat(amount); 
		fundTransferPage.isFundTransferSuccessDisplay();		
	}
	
	@Test
	public void TC_07_BalanceEnquiry() {			
		balanceEnquiryPage=fundTransferPage.openBalanceEnquiryLink(driver);
		balanceEnquiryPage.inputToAccountNoTextbox(accountID);
		balanceEnquiryPage.clickSubmitBalanceEnquiryButton();
		balanceEnquiryPage.isBalanceEnquirySuccessDisplay(accountID);
		String balance = balanceEnquiryPage.getBalanceText();
		verifyEqual(totalAmount, Float.parseFloat(balance));
		System.out.println(accountID);
		System.out.println(customerID);
	}
	
	@Test
	public void TC_08_DeleteAccount() {			
		deleteAccountPage=balanceEnquiryPage.openDeleteAccountLink(driver);
		deleteAccountPage.inputToAccountNoTextbox(driver, accountID);
		deleteAccountPage.clickSubmitDeleteAccountButton(driver);
		verifyEqual(DeleteAccountPageUI.DELETEDACCOUNTSUCCESS_MESSAGE, deleteAccountPage.getTextAlertDeleteAccount(driver));
		deleteAccountPage.acceptAlert(driver);
	}
	
	@Test
	public void TC_09_DeleteCustomer() {			
		deleteCustomerPage=deleteAccountPage.openDeleteCustomerLink(driver);
		deleteCustomerPage.inputToCustomerIDTextbox(driver, customerID);
		deleteCustomerPage.clickSubmitDeleteCustomerButton(driver);
		verifyEqual(DeleteCustomerPageUI.DELETEDCUSTOMERSUCCESS_MESSAGE, deleteCustomerPage.getTextAlertDeleteCustomer(driver));
		deleteCustomerPage.acceptAlert(driver);
	}
	@AfterClass
	public void afterClass() {
		quitBrowser(driver);
	}
}
