package com.bankguru.account;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import commons.AbstractPage;
import commons.Constants;



public class Level_02_RegisterAndLoginLogOut_AbstractPage {
	private WebDriver driver;
	private String email,userID,password,loginPageUrl;
	private commons.AbstractPage abtractPage;
	
	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		abtractPage =new AbstractPage();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		abtractPage.openAnyURL(driver, Constants.DEV_URL);
		email = "kdc"+randomNumber()+"@gmail.com";
	}

	@Test
	public void TC_01_RegisterToSystem() {
		abtractPage.clickToElement(driver, "//a[text()='here']");
		abtractPage.senkeyToElement(driver, "//input[@name ='emailid']", email);
		abtractPage.clickToElement(driver, "//input[@name='btnLogin']");
		userID=abtractPage.getTextElement(driver, "//td[text()='User ID :']/following-sibling::td");
		password=abtractPage.getTextElement(driver, "//td[text()='Password :']/following-sibling::td");
	}
	
	@Test
	public void TC_02_LoginToSystem() {
		//mngr162679
		//rAjUtun
		abtractPage.openAnyURL(driver, Constants.DEV_URL);
		abtractPage.senkeyToElement(driver, "//input[@name='uid']", userID);
		abtractPage.senkeyToElement(driver, "//input[@name='password']", password);
		abtractPage.clickToElement(driver, "//input[@name='btnLogin']");
		Assert.assertTrue(abtractPage.isControlDisplay(driver, "//marquee[text()=\"Welcome To Manager's Page of Guru99 Bank\"]"));
		Assert.assertTrue(abtractPage.isControlDisplay(driver, "//td[text()='Manger Id : "+userID+"']"));
	}
	
	@Test
	public void TC_03_LogOutToSystem() {		
		abtractPage.clickToElement(driver, "//a[text()='Log out']");		
		abtractPage.acceptAlert(driver);		
		Assert.assertTrue(abtractPage.isControlDisplay(driver, "//form[@name='frmLogin']"));
	}

	@AfterClass
	public void afterClass() {
		
	}
	
	public int randomNumber()
	{
		Random random =new Random();
		int number = random.nextInt(999999);
		return number;
	}

}
