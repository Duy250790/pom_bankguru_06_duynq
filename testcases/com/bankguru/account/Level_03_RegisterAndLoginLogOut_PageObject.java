package com.bankguru.account;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import commons.AbstractPage;
import commons.AbstractTest;
import commons.Constants;
import commons.PageFactoryManage;
import pages.DepositPageObject;
import pages.EditCustomerPageObject;
import pages.FundTransferPageObject;
import pages.HomePageObject;
import pages.LoginPageObject;
import pages.NewCustomerPageObject;
import pages.RegisterPageObject;



public class Level_03_RegisterAndLoginLogOut_PageObject extends AbstractTest {
	private WebDriver driver;
	private String email,userID,password,loginPageUrl;
	private commons.AbstractPage abtractPage;
	private pages.RegisterPageObject registerPage;
	private pages.HomePageObject homePage;
	private pages.LoginPageObject loginPage;
	private pages.NewCustomerPageObject newCustomerPage;
	private pages.EditCustomerPageObject editCustomerPage;
	private pages.DepositPageObject depositPage;
	private pages.FundTransferPageObject funTransferPage;
	
	
	@Parameters("browser")
	@BeforeClass
	public void beforeClass(String BrowserName)  {		
		driver=OpenMultiBrowser(BrowserName);
		loginPage = PageFactoryManage.getLoginPage(driver);
		System.out.println(driver.toString().toLowerCase()); 
		email = "kdc"+randomNumber()+"@gmail.com";
	}

	@Test
	public void TC_01_RegisterToSystem() {			
		loginPageUrl=loginPage.getLoginPageURL();		
		registerPage=loginPage.clickToHereLink();
		registerPage.inputToEmailTextbox(email);
		registerPage.clickSubmitButton();
		userID=registerPage.getUserIDText();
		password=registerPage.getPasswordText();
	}
	
	@Test
	public void TC_02_LoginToSystem() {			
		loginPage=registerPage.openLoginPage(loginPageUrl);		
		loginPage.inputToUserIDTextbox(userID);
		loginPage.inputToPasswordTextbox(password);
		homePage=loginPage.clickToLoginButton();
		Assert.assertTrue(homePage.isHomePageDisplay());
	}
	
	@Test
	public void TC_03_OpenMultiPage() {
		newCustomerPage=homePage.openNewCustomerLink(driver);
		editCustomerPage=newCustomerPage.openEditCustomerLink(driver);
		depositPage=editCustomerPage.openDepositCustomerLink(driver);
		funTransferPage=depositPage.openFundTransferLink(driver);	
	}
	
	@Test
	public void TC_04_LogOutToSystem() {		
		loginPage=funTransferPage.clickToLogoutLink(driver);
		Assert.assertTrue(loginPage.isLoginFormDisplay());		
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}
	
	

}
