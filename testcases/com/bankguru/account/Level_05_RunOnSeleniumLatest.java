package com.bankguru.account;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import commons.AbstractPage;
import commons.AbstractTest;
import commons.Constants;
import commons.PageFactoryManage;
import pages.DepositPageObject;
import pages.EditCustomerPageObject;
import pages.FundTransferPageObject;
import pages.HomePageObject;
import pages.LoginPageObject;
import pages.NewCustomerPageObject;
import pages.RegisterPageObject;



public class Level_05_RunOnSeleniumLatest extends AbstractTest {
	private WebDriver driver;
	private String email,userID,password,loginPageUrl;
	private commons.AbstractPage abtractPage;
	private pages.RegisterPageObject registerPage;
	private pages.HomePageObject homePage;
	private pages.LoginPageObject loginPage;
	private pages.NewCustomerPageObject newCustomerPage;
	private pages.EditCustomerPageObject editCustomerPage;
	private pages.DepositPageObject depositPage;
	private pages.FundTransferPageObject funTransferPage;
	
	
	@Parameters("browser")
	@BeforeClass
	public void beforeClass(String BrowserName)  {		
		driver=OpenMultiBrowser(BrowserName);
		loginPage = PageFactoryManage.getLoginPage(driver);
		email = "kdc"+randomNumber()+"@gmail.com";
	}

	@Test
	public void TC_01_RegisterToSystem() {		
		log.info("------------Start Test Case: TC_01_RegisterToSystem-----------------");
		log.info("Account - TC 01: Step 01 - Get Login Page URL");
		loginPageUrl=loginPage.getLoginPageURL();
		
		log.info("Account - TC 01: Step 02 - Click Here Link");
		registerPage=loginPage.clickToHereLink();
		
		log.info("Account - TC 01: Step 03 - Input to Email TextBox ");
		registerPage.inputToEmailTextbox(email);
		
		log.info("Account - TC 01: Step 04 - Click submit Button");
		registerPage.clickSubmitButton();
		
		log.info("Account - TC 01: Step 05 - Get User and PassWord");
		userID=registerPage.getUserIDText();
		password=registerPage.getPasswordText();
		
		log.info("------------End Test Case: TC_01_RegisterToSystem-----------------" + "\n");
	}
	
	@Test
	public void TC_02_LoginToSystem() {			
		log.info("------------Start Test Case: TC_02_LoginToSystem----------------- ");
		log.info("Account - TC 02: Step 01 - Open login Page");
		loginPage=registerPage.openLoginPage(loginPageUrl);	
		
		log.info("Account - TC 02: Step 02 - Verify Login Page Display");
		verifyTrue(loginPage.isLoginFormDisplay());
		
		log.info("Account - TC 02: Step 03 - Input to UserID");
		loginPage.inputToUserIDTextbox(userID);
		
		log.info("Account - TC 02: Step 04 - Input to Password");
		loginPage.inputToPasswordTextbox(password);
		
		log.info("Account - TC 02: Step 05 - Click to Login Button");
		homePage=loginPage.clickToLoginButton();
		
		log.info("Account - TC 02: Step 06 - Verify HomepageDisplay");
		verifyTrue(homePage.isHomePageDisplay());
		
		log.info("------------End Test Case: TC_02_LoginToSystem-----------------" + "\n");
	}
	
	@Test(enabled =false)
	public void TC_03_OpenMultiPage() {
		newCustomerPage=homePage.openNewCustomerLink(driver);
		editCustomerPage=newCustomerPage.openEditCustomerLink(driver);
		depositPage=editCustomerPage.openDepositCustomerLink(driver);
		funTransferPage=depositPage.openFundTransferLink(driver);	
	}
	
	@Test(enabled =false)
	public void TC_04_LogOutToSystem() {		
		loginPage=funTransferPage.clickToLogoutLink(driver);
		verifyTrue(loginPage.isLoginFormDisplay());		
	}

	@AfterClass
	public void afterClass() {
		quitBrowser(driver);
	}
	
	

}
